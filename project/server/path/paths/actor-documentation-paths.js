
'use strict';

const ActorPathDocumentation = require('../actor-path-documentation');
const ActorPathCreator = require('z-abs-corelayer-server/server/path/actor-path-creator');
const ActorPath = require('z-abs-corelayer-server/server/path/actor-path');
const Fs = require('fs');
const Path = require('path');


class ActorDocumentationPaths {
  constructor() {
    this.folderPaths = [];
    this.filePaths = [];
    if(ActorPath.releaseStepDevelopment) {
      this.folderPaths.push(ActorPathDocumentation.getDocumentationLocalBin());
      this.folderPaths.push(ActorPathDocumentation.getDocumentationLocalText());
      this.folderPaths.push(ActorPathDocumentation.getDocumentationLocalFolder('Documentation'));
      
      this.filePaths.push({
        path: `${ActorPathDocumentation.getDocumentationLocalBin()}${Path.sep}.gitattributes`,
        text: this._getGitAttributes()
      });
      this.filePaths.push({
        path: `${ActorPathDocumentation.getDocumentationLocalBin()}${Path.sep}.gitignore`,
        text: this._getGitIgnore()
      });
      this.filePaths.push({
        path: `${ActorPathDocumentation.getDocumentationLocalText()}${Path.sep}.gitignore`,
        text: this._getGitIgnore()
      });
      this.filePaths.push({
        path: ActorPathDocumentation.getDocumentationReferences('repo'),
        default: []
      });
      this.filePaths.push({
        path: ActorPathDocumentation.getDocumentationNavigationFile('actorjs', 'Documentation', 'local'),
        default: []
      });
    }
  }
  
  verifyOrCreate(done) {
    new ActorPathCreator(this.folderPaths, this.filePaths).verifyOrCreate(done);
  }
  
  _getGitIgnore() {
    return '';
  }
  
  _getGitAttributes() {
    return `
# Set the default behavior, in case people don't have core.autocrlf set.
* text=auto

# Explicitly declare text files you want to always be normalized and converted
# to native line endings on checkout.
#*.c text
#*.h text

# Declare files that will always have CRLF line endings on checkout.
#*.sln text eol=crlf

# Declare files that will always have LF line endings on checkout.
*.txt text eol=lf

# Denote all files that are truly binary and should not be modified.
*.png binary
*.jpg binary
*.gif binary
*.bmp binary
*.tif binary
*.tiff binary
*.psd binary
*.mp4 binary
*.mkv binary
*.avi binary
*.mov binary
*.vob binary
*.mp3 binary
*.aac binary
*.wav binary
*.flac binary
*.ogg binary
*.mka binary
*.wma binary
*.pdf binary
*.doc binary
*.xla binary
*.ppt binary
*.docx binary
*.odt binary
*.zip binary
*.rar binary
*.7z binary
*.tar binary
*.iso binary
`;
  }
}

module.exports = ActorDocumentationPaths;
