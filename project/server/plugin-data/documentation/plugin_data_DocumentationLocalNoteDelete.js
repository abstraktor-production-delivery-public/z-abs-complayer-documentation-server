
'use strict';

const ActorPathData = require('z-abs-corelayer-server/server/path/actor-path-data');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class DocumentationLocalNoteDelete extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.DELETE);
  }
  
  onRequest(guid, path) {
    this.asynchRmFileResponse(ActorPathData.getDocumentationNoteLocalFile(guid, path));
  }
}


module.exports = DocumentationLocalNoteDelete;
