
'use strict';

const ActorPathDocumentation = require('../../path/actor-path-documentation');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class DocumentationReferencesGet extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
  }
  
  onRequest(repo) {
    this.asynchReadFileResponse(ActorPathDocumentation.getDocumentationReferences(repo));
  }
}


module.exports = DocumentationReferencesGet;
