
'use strict';

const StackHelper = require('./stack-helper');
const ActorPathDocumentation = require('../../path/actor-path-documentation');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');
const Fs = require('fs');


class DocumentationGet extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
  }
  
  onRequest(repo, appName, documentGroup, documentName, embed) {
    if(!embed) {
      this._getNavigationAndDocument(repo, appName, documentGroup, documentName);
    }
    else {
      if(Array.isArray(documentName)) {
        this._getDocuments(repo, documentGroup, documentName, DocumentationGet.NEW_EMBEDED_LINK);
      }
      else {
        this._getDocument(ActorPathDocumentation.getDocumentationFile(documentGroup, documentName, repo), DocumentationGet.NEW_EMBEDED_LINK);
      }
    }
  }
  
  _getDocument(document, notFoundText) {
    const index1 = this.expectAsynchResponse();
    const index2 = this.expectAsynchResponse();
    Fs.readFile(document, 'utf8', (err, data) => {
      if(err) {
        this.asynchResponseSuccess(index1, notFoundText);
        this.asynchResponseSuccess(index2, false);
      }
      else {
        this.asynchResponseSuccess(index1, data);
        this.asynchResponseSuccess(index2, true);
      }
    });
  }
  
  _getDocuments(repo, documentGroup, documentNames, notFoundText) {
    let pendings = documentNames.length;
    if(0 === pendings) {
      process.nextTick(() => {
        this.expectAsynchResponseSuccess();
      });
      return;
    }
    const index1 = this.expectAsynchResponse();
    const index2 = this.expectAsynchResponse();
    const index3 = this.expectAsynchResponse();
    const receivedDcuments = [];
    const documentNamesFound = [];
    const documentsFound = [];
    documentNames.forEach((documentName) => {
      const currentDocumentName = documentName;
      Fs.readFile(ActorPathDocumentation.getDocumentationFile(documentGroup, documentName, repo), 'utf8', (err, data) => {
        documentNamesFound.push(currentDocumentName);
        if(err) {
          receivedDcuments.push(notFoundText);
          documentsFound.push(false);
        }
        else {
          receivedDcuments.push(data);
          documentsFound.push(true);
        }
        if(0 === --pendings) {
          this.asynchResponseSuccess(index1, receivedDcuments);
          this.asynchResponseSuccess(index2, documentNamesFound);
          this.asynchResponseSuccess(index3, documentsFound);
        }
      });
    });  
  }
  
  _getNavigationAndDocument(repo, appName, documentGroup, documentName) {
    const index1 = this.expectAsynchResponse();
    const match = documentName ? documentName.match(/^\[\[stack\-[a-z-]+\]\]/) : null;
    Fs.readFile(ActorPathDocumentation.getDocumentationNavigationFile(appName, documentGroup, repo), (err, data) => {
      if(err) {
        this.asynchResponseError(`Could not get DocumentationNavigationGlobalFile. '${ActorPathDocumentation.getDocumentationNavigationFile(appName, documentGroup, repo)}'`, index1);
      }
      else {
        const navigations = JSON.parse(data);
        navigations.findIndex((navigation) => {
          const refIndex = navigation.refs.findIndex((ref) => {
            return ref.name === '[[stack]]';
          });
          if(-1 !== refIndex) {
            this._getStackNavigation(navigation, () => {
              if(documentName && match) {
                StackHelper.file(match, (file) => {
                  if(null !== file) {
                    this._getDocument(file, DocumentationGet.NEW_STACK);
                  }
                  else {
                    this.asynchResponseError(`GetStacksObject not registered'.`, index1);
                  }
               });
              }
            });
            return true;
          }
          else {
            return false;
          }
        });
        if(!documentName) {
          if(0 !== navigations.length && navigations[0].refs.length) {
            this._getDocument(ActorPathDocumentation.getDocumentationFile(documentGroup, navigations[0].refs[0].link, repo), DocumentationGet.NEW_DOCUMENT);
          }
          else {
            this.expectAsynchResponseSuccess(DocumentationGet.NEW_DOCUMENT);
            this.expectAsynchResponseSuccess(false);
          }
        }
        this.asynchResponseSuccess(index1, navigations);
      }
    });
    if(documentName && !match) {
      this._getDocument(ActorPathDocumentation.getDocumentationFile(documentGroup, documentName, repo), DocumentationGet.NEW_DOCUMENT);
    }
  }
  
  _getStackNavigation(navigation, done) {
    this.expectAsynchResponseTemp();
    navigation.refs = [];
    StackHelper.getStacks((stacks) => {
      stacks.forEach((value, key) => {
        navigation.refs.push({
          name: key,
          link: `[[stack-${key}]]`
        });
      });
      done();
      this.unExpectAsynchResponseTemp();
    });
  }
}

DocumentationGet.NEW_DOCUMENT = '# **NEW DOCUMENT**\n## Please Edit: ***`TO BE DONE !!!`***\r\n';
DocumentationGet.NEW_EMBEDED_LINK = '## **EMBEDED LINK NOT CREATED YET.**\r\n';
DocumentationGet.NEW_STACK = '## **STACK DOCUMENTATION NOT CREATED YET**\n## Please Edit: ***`TO BE DONE !!!`***\r\n';


module.exports = DocumentationGet;
