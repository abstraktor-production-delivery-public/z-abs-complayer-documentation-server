
'use strict';

const StackHelper = require('./stack-helper');
const ActorPathDocumentation = require('../../path/actor-path-documentation');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


class DocumentationUpdate extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.UPDATE);
  }
  
  onRequest(repo, documentGroup, documentName, documentData) {
    const match = documentName ? documentName.match(/^\[\[stack\-[a-z-]+\]\]/) : null;
    if(!match) {
      this.asynchWriteTextFileResponse(ActorPathDocumentation.getDocumentationFile(documentGroup, documentName, repo), documentData);
    }
    else {
      StackHelper.file(match, (file) => {
        if(null !== file) {
          this.asynchWriteTextFileResponse(file, documentData);
        }
        else {
          this.responsePartError(`GetStacksObject not registered'.`);
        }
      });
    }
  }
}


module.exports = DocumentationUpdate;
