
'use strict';

const ActorPath = require('z-abs-corelayer-server/server/path/actor-path');
const Path = require('path');


class ActorPathDocumentation {
  constructor() {
    this.actorDocumentationPathLocal = ActorPath.setPath(ActorPath.getActorPath(), `${Path.sep}..${Path.sep}Documentation`);
    if(ActorPath.releaseStepDelivery) {
      this.actorDocumentationPathGlobal = ActorPath.setPath(ActorPath.getActorPath(), `${Path.sep}node_modules${Path.sep}${ActorPath.organization}`);
    }
    else if(ActorPath.releaseStepCandidate) {
      this.actorDocumentationPathGlobal = ActorPath.setPath(ActorPath.getActorPath(), `${Path.sep}node_modules`);
    }
    else {
      this.actorDocumentationPathGlobal = ActorPath.setPath(ActorPath.getActorPath(), `${Path.sep}..${Path.sep}Documentation`);
    }
    this.actorDocumentationGlobalBin = ActorPath.setPath(this.actorDocumentationPathGlobal, `${Path.sep}actorjs-documentation-bin`);
    this.actorDocumentationGlobalText = ActorPath.setPath(this.actorDocumentationPathGlobal, `${Path.sep}actorjs-documentation-text`);
    
    this.actorDocumentationLocalBin = ActorPath.setPath(this.actorDocumentationPathLocal, `${Path.sep}local-documentation-bin`);
    this.actorDocumentationLocalText = ActorPath.setPath(this.actorDocumentationPathLocal, `${Path.sep}local-documentation-text`);
  }
  
  getDocumentationLocalBin() {
    return this.actorDocumentationLocalBin;
  }
  
  getDocumentationLocalText() {
    return this.actorDocumentationLocalText;
  }
  
  getDocumentationReferences(repo) {
    if(!repo) {
      return `${this.actorDocumentationGlobalText}${Path.sep}Documentation${Path.sep}documentation-references.json`;
    }
    else {
      return `${this.actorDocumentationLocalText}${Path.sep}Documentation${Path.sep}documentation-references.json`;
    }
  }
  
  getDocumentationGlobalFolder(documentGroup) {
    return `${this.actorDocumentationGlobalText}${Path.sep}${documentGroup}`;
  }
  
  getDocumentationLocalFolder(documentGroup) {
    return `${this.actorDocumentationLocalText}${Path.sep}${documentGroup}`;
  }
  
  getDocumentationNavigationFile(appName, documentGroup, repo) {
    if(!repo) {
      return `${this.getDocumentationGlobalFolder(documentGroup)}${Path.sep}Navigation-${documentGroup}-${appName}-global.txt`;
    }
    else {
      return `${this.getDocumentationLocalFolder(documentGroup)}${Path.sep}Navigation-${documentGroup}-${appName}-${repo}.txt`;
    }
  }
  
  getDocumentationFilePathAndFile(documentGroup, documentName, repo) {
    const formattedDocumentName = Path.normalize(documentName.replace(new RegExp('[/\\\\]', 'g'), Path.sep));
    const pathSplit = formattedDocumentName.split(Path.sep);
    const splitteredDocumentName = pathSplit[pathSplit.length - 1];
    let splitteredPath = '';
    if(1 !== pathSplit.length) {
      pathSplit.splice(pathSplit.length - 1, 1);
      splitteredPath = pathSplit.join(Path.sep);
    }
    if(!repo) {
      return {
        path: `${this.actorDocumentationGlobalText}${Path.sep}${documentGroup}${Path.sep}${splitteredPath}`,
        file: `${this.actorDocumentationGlobalText}${Path.sep}${documentGroup}${Path.sep}${splitteredPath}${Path.sep}Document-global-${splitteredDocumentName}.txt`
      };
    }
    else {
      return {
        path: `${this.actorDocumentationLocalText}${Path.sep}${documentGroup}${Path.sep}${splitteredPath}`,
        file: `${this.actorDocumentationLocalText}${Path.sep}${documentGroup}${Path.sep}${splitteredPath}${Path.sep}Document-${repo}-${splitteredDocumentName}.txt`
      };
    }
  }
  
  getDocumentationFile(documentGroup, documentName, repo) {
    return this.getDocumentationFilePathAndFile(documentGroup, documentName, repo).file;
  }
}


module.exports = new ActorPathDocumentation();
