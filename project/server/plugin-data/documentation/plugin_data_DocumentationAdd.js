
'use strict';

const StackHelper = require('./stack-helper');
const ActorPathDocumentation = require('../../path/actor-path-documentation');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');


// TODO: synchronize access to the file
class DocumentationAdd extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.ADD);
  }
  
  onRequest(repo, documentGroup, documentName, documentData) {
    const match = documentName ? documentName.match(/^\[\[stack\-[a-z-]+\]\]/) : null;
    if(!match) {
      this._save(ActorPathDocumentation.getDocumentationFilePathAndFile(documentGroup, documentName, repo), documentName, documentData);
    }
    else {
      StackHelper.pathAndFile(match, (pathAndFile) => {
        this._save(pathAndFile, documentName, documentData);
      });
    }
  }
  
  _save(pathAndFile, documentName, documentData) {
    if(null !== pathAndFile) {
      this.asynchMkdirResponse(pathAndFile.path, (err) => {
        if(!err) {
          this.asynchWriteTextFileResponse(pathAndFile.file, documentData);
        }
        else {
          this.responsePartError(`Could not create document path '${documentName}'.`);
        }
      }, true);
    }
    else {
      this.responsePartError(`GetStacksObject not registered'.`);
    }
  }
}


module.exports = DocumentationAdd;
