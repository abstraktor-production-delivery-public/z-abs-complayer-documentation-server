
'use strict';

const ActorPathDocumentation = require('../../path/actor-path-documentation');
const PluginBaseMulti = require('z-abs-corelayer-server/server/plugin-base-multi');
const Fs = require('fs');


class DocumentationNavigationsUpdate extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.UPDATE);
  }
  
  onRequest(repo, appName, documentGroup, navigations) {
    Fs.writeFile(ActorPathDocumentation.getDocumentationNavigationFile(appName, documentGroup, repo), JSON.stringify(navigations, null, 2), (err) => {
      if(err) {
        return this.responsePartError(`Could not update the Document Navigation File: ' ${ActorPathDocumentation.getDocumentationNavigationFile(appName, documentGroup, repo)} '.`);
      }
      return this.responsePartSuccess();
    });
  }
}


module.exports = DocumentationNavigationsUpdate;
